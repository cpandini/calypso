First barebone project for Calypso based on Athena master. Only packages not included in atlas/athena are included. The current build relies on the entire ATLAS software library. 

The following sequence will allow you to compile Calypso 1.0.0 on any machine with cvmfs access.
```
#clone the (forked) project to your local machine
git clone https://:@gitlab.cern.ch:8443/$USERNAME/calypso.git 


#The next three lines are used to setup the ATLAS release environment
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase 
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup --input=calypso/asetup.faser master,latest,Athena

#create build directory
mkdir build
cd build
#build calypso
cmake -DCMAKE_INSTALL_PREFIX=../run ../calypso ; make ; make install

It can be convenient to alias the "asetup --input=calypso/asetup.faser" to something like "fsetup"