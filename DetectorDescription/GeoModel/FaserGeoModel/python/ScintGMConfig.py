#
#  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def ScintGeometryCfg (flags):
    acc = ComponentAccumulator()
    from VetoGeoModel.VetoGeoModelConfig import VetoGeometryCfg
    acc.merge(VetoGeometryCfg( flags ))

    from TriggerGeoModel.TriggerGeoModelConfig import TriggerGeometryCfg
    acc.merge(TriggerGeometryCfg( flags ))
    
    from PreshowerGeoModel.PreshowerGeoModelConfig import PreshowerGeometryCfg
    acc.merge(PreshowerGeometryCfg( flags ))
    

    return acc


if __name__ == "__main__":
  # import os
  from AthenaCommon.Logging import log
  from AthenaCommon.Constants import DEBUG
  from AthenaCommon.Configurable import Configurable
  from AthenaConfiguration.AllConfigFlags import ConfigFlags
  from AthenaConfiguration.MainServicesConfig import MainServicesCfg
  from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
  # Set up logging and new style config
  log.setLevel(DEBUG)
  Configurable.configurableRun3Behavior = True
  ConfigFlags.addFlag("GeoModel.FaserVersion", "Faser-01")
  # ConfigFlags.addFlag("Detector.SimulateVeto", True)

  # from AthenaConfiguration.TestDefaults import defaultTestFiles
  # Provide MC input
  # ConfigFlags.Input.Files = defaultTestFiles.HITS
  ConfigFlags.IOVDb.GlobalTag = "OFLCOND-XXXX-XXX-XX"
  ConfigFlags.Detector.SimulateVeto   = True
  ConfigFlags.Detector.SimulateTrigger= True
  ConfigFlags.Detector.SimulatePreshower= True
  ConfigFlags.GeoModel.Align.Dynamic    = False
  # Provide data input
  ##from AthenaConfiguration.TestDefaults import defaultTestFiles
  #
  ConfigFlags.lock()
  # Construct ComponentAccumulator
  acc = MainServicesCfg(ConfigFlags)
  acc.merge(PoolReadCfg(ConfigFlags))
  acc.merge(ScintGeometryCfg(ConfigFlags)) # FIXME This sets up the whole Scint geometry would be nicer just to set up min required
  #acc.getService("StoreGateSvc").Dump=True
  acc.getService("ConditionStore").Dump=True
  acc.printConfig(withDetails=True)
  f=open('ScintGMCfg2.pkl','w')
  acc.store(f)
  f.close()
  ConfigFlags.dump()
  # Execute and finish
  acc.run(maxEvents=3)
