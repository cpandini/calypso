#
#  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from FaserSCT_GeoModel.FaserSCT_GeoModelConfig import FaserSCT_GeometryCfg

def SctGeometryCfg (flags):
    acc = ComponentAccumulator()
    acc.merge(FaserSCT_GeometryCfg( flags ))
    return acc


if __name__ == "__main__":
  from AthenaCommon.Logging import log
  from AthenaCommon.Constants import DEBUG
  from AthenaCommon.Configurable import Configurable
  from CalypsoConfiguration.AllConfigFlags import ConfigFlags
  from AthenaConfiguration.MainServicesConfig import MainServicesCfg
  from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
  # Set up logging and new style config
  log.setLevel(DEBUG)
  Configurable.configurableRun3Behavior = True
  ConfigFlags.addFlag("GeoModel.FaserVersion", "Faser-01")
  # ConfigFlags.addFlag("Detector.SimulateVeto", True)
  # ConfigFlags.addFlag("Detector.SimulateFaserSCT", True)

  # from AthenaConfiguration.TestDefaults import defaultTestFiles
  # Provide MC input
  # ConfigFlags.Input.Files = defaultTestFiles.HITS
  ConfigFlags.IOVDb.GlobalTag = "OFLCOND-XXXX-XXX-XX"
  ConfigFlags.Detector.SimulateFaserSCT = True
  ConfigFlags.GeoModel.Align.Dynamic    = False
  # Provide data input
  ##from AthenaConfiguration.TestDefaults import defaultTestFiles
  #
  ConfigFlags.lock()
  # Construct ComponentAccumulator
  acc = MainServicesCfg(ConfigFlags)
  acc.merge(PoolReadCfg(ConfigFlags))
  # acc.merge(ScintGeometryCfg(ConfigFlags)) 
  acc.merge(FaserSCT_GeometryCfg(ConfigFlags)) 
  #acc.getService("StoreGateSvc").Dump=True
  acc.getService("ConditionStore").Dump=True
  acc.printConfig(withDetails=True)
  f=open('TrackerGMCfg2.pkl','w')
  acc.store(f)
  f.close()
  ConfigFlags.dump()
  # Execute and finish
  acc.run(maxEvents=3)
