################################################################################
# Package: TrackerPrepRawDataFormation
################################################################################

# Declare the package name:
atlas_subdir( TrackerPrepRawDataFormation )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthenaBaseComps
                          Control/StoreGate
                          DetectorDescription/Identifier
                          GaudiKernel
                          Tracker/TrackerRawEvent/TrackerRawData
                          Tracker/TrackerRecEvent/TrackerPrepRawData
                          Tracker/TrackerRecTools/FaserSiClusterizationTool
                          PRIVATE
                          Control/AthViews
                          DetectorDescription/FaserDetDescr
                          Tracker/TrackerDetDescr/TrackerIdentifier
                          Tracker/TrackerDetDescr/TrackerReadoutGeometry
                          Tracker/TrackerConditions/FaserSCT_ConditionsData
                          InnerDetector/InDetConditions/SCT_ConditionsData
                          InnerDetector/InDetConditions/InDetConditionsSummaryService
                        )

# Component(s) in the package:
atlas_add_component( TrackerPrepRawDataFormation
                     src/*.cxx src/*.h
                     src/components/*.cxx
                     LINK_LIBRARIES AthenaBaseComps StoreGateLib SGtests Identifier GaudiKernel TrackerRawData TrackerPrepRawData FaserSiClusterizationToolLib FaserDetDescr TrackerIdentifier TrackerReadoutGeometry AthViews)

# Install files from the package:
#atlas_install_headers( TrackerPrepRawDataFormation )

atlas_install_python_modules( python/*.py )

atlas_install_scripts( test/*.py )
