/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
   */

/***************************************************************************
  -------------------
  ATLAS Collaboration
 ***************************************************************************/

//<<<<<< INCLUDES >>>>>>


#include "TruthSeededTrackFinder.h"

// For processing clusters
#include "TrackerReadoutGeometry/SiLocalPosition.h"
#include "TrackerReadoutGeometry/SiDetectorDesign.h"
#include "TrackerReadoutGeometry/SiDetectorElement.h"

// Space point Classes,
#include "TrkSpacePoint/SpacePointCollection.h"
// #include "TrkSpacePoint/SpacePointCLASS_DEF.h"
#include "TrackerIdentifier/FaserSCT_ID.h"


// general Atlas classes
#include "FaserDetDescr/FaserDetectorID.h"
#include "xAODEventInfo/EventInfo.h"
#include "StoreGate/ReadCondHandle.h"

#include "AthenaMonitoringKernel/Monitored.h"

namespace Tracker
{

//------------------------------------------------------------------------
TruthSeededTrackFinder::TruthSeededTrackFinder(const std::string& name,
    ISvcLocator* pSvcLocator)
  : AthReentrantAlgorithm(name, pSvcLocator)
  , m_hist_n(0)
  , m_hist_x(0)
  , m_hist_y(0)
  , m_hist_z(0)
  , m_hist_r(0)
  , m_hist_phi(0)
  , m_hist_eta(0)
  , m_hist_layer(0)
  , m_hist_strip(0)
  , m_hist_station(0)
  , m_hist_x_y_plane0(0)
  , m_hist_x_y_plane1(0)
  , m_hist_x_y_plane2(0)
  , m_hist_x_y_plane3(0)
  , m_hist_x_y_plane4(0)
  , m_hist_x_y_plane5(0)
  , m_hist_x_y_plane6(0)
  , m_hist_x_y_plane7(0)
  , m_hist_x_y_plane8(0)
  , m_hist_x_z(0)
  , m_hist_y_z(0)
  , m_hist_sp_sensor(0)
  , m_hist_sp_module (0)
  , m_hist_sp_row (0)
  , m_hist_sp_plane (0)
  , m_hist_sp_layer (0)
  , m_hist_sp_station (0)

  , m_thistSvc("THistSvc", name)
{ 
}

//-----------------------------------------------------------------------
StatusCode TruthSeededTrackFinder::initialize()
{
  //
  ATH_MSG_DEBUG( "TruthTrackSeeds::initialize()" );

  CHECK(m_thistSvc.retrieve());
  // Check that clusters, space points and ids have names
  if ( m_Sct_spcontainerKey.key().empty()){
    ATH_MSG_FATAL( "SCTs selected and no name set for SCT clusters");
    return StatusCode::FAILURE;
  }
  ATH_CHECK( m_Sct_spcontainerKey.initialize() );

  if ( m_seed_spcontainerKey.key().empty()){
    ATH_MSG_FATAL( "No name set for output seeds space points");
    return StatusCode::FAILURE;
  }

  ATH_CHECK( m_seed_spcontainerKey.initialize() );

  // create containers (requires the Identifier Helpers)
  ATH_CHECK(detStore()->retrieve(m_idHelper,"FaserSCT_ID"));

  // Initialize the key of input SiElementPropertiesTable and SiDetectorElementCollection for SCT
  ATH_CHECK(m_SCTDetEleCollKey.initialize());

  ATH_CHECK( m_mcEventKey.initialize() );
  ATH_CHECK( m_faserSiHitKey.initialize() );
  ATH_CHECK( m_faserRdoKey.initialize());
  ATH_CHECK( m_sctMap.initialize());
  ATH_MSG_INFO( "Using GenEvent collection with key " << m_mcEventKey.key());
  ATH_MSG_INFO( "Using Faser SiHit collection with key " << m_faserSiHitKey.key());
  ATH_MSG_INFO( "Using FaserSCT RDO Container with key " << m_faserRdoKey.key());
  ATH_MSG_INFO( "Using SCT_SDO_Map with key "<< m_sctMap.key());

  m_hist_n=new TH1D("sp_n","sp_n",20,0,20);
  m_hist_x=new TH1D("sp_x","sp_x",100,-200,200);
  m_hist_y=new TH1D("sp_y","sp_y",100,-200,200);
  m_hist_z=new TH1D("sp_z","sp_z",3500,0,3500);
  m_hist_r=new TH1D("sp_r","sp_r",100,0,200);
  m_hist_eta=new TH1D("sp_eta","sp_eta",100,0,5);
  m_hist_phi=new TH1D("sp_phi","sp_phi",100,-3.2,3.2);
  m_hist_strip=new TH1D("sp_strip","sp_strip",1000,0,1000);
  m_hist_layer=new TH1D("sp_layer","sp_layer",100,-10,10);
  m_hist_station=new TH1D("sp_station","sp_station",100,-10,10);
  m_hist_sp_station=new TH1D("sp_all_station","sp_station",100,-10,10);
  m_hist_sp_row=new TH1D("sp_all_row","sp_station",100,-10,10);
  m_hist_sp_module=new TH1D("sp_all_module","sp_station",100,-10,10);
  m_hist_sp_sensor=new TH1D("sp_all_sensor","sp_station",100,-10,10);
  m_hist_sp_plane=new TH1D("sp_all_plane","sp_station",100,-10,10);
  m_hist_sp_layer=new TH1D("sp_all_layer","sp_station",100,-10,10);
  m_hist_x_z=new TH2D("sp_x_z","sp_x_z",100,-200,200,3500,0,3500);
  m_hist_y_z=new TH2D("sp_y_z","sp_y_z",100,-200,200,3500,0,3500);
  m_hist_x_y_plane0=new TH2D("sp_x_y_plane0","sp_x_y_plane0",100,-200,200,100,-200,200);
  m_hist_x_y_plane1=new TH2D("sp_x_y_plane1","sp_x_y_plane1",100,-200,200,100,-200,200);
  m_hist_x_y_plane2=new TH2D("sp_x_y_plane2","sp_x_y_plane2",100,-200,200,100,-200,200);
  m_hist_x_y_plane3=new TH2D("sp_x_y_plane3","sp_x_y_plane3",100,-200,200,100,-200,200);
  m_hist_x_y_plane4=new TH2D("sp_x_y_plane4","sp_x_y_plane4",100,-200,200,100,-200,200);
  m_hist_x_y_plane5=new TH2D("sp_x_y_plane5","sp_x_y_plane5",100,-200,200,100,-200,200);
  m_hist_x_y_plane6=new TH2D("sp_x_y_plane6","sp_x_y_plane6",100,-200,200,100,-200,200);
  m_hist_x_y_plane7=new TH2D("sp_x_y_plane7","sp_x_y_plane7",100,-200,200,100,-200,200);
  m_hist_x_y_plane8=new TH2D("sp_x_y_plane8","sp_x_y_plane8",100,-200,200,100,-200,200);
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_n",m_hist_n));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x",m_hist_x));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_y",m_hist_y));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_z",m_hist_z));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_r",m_hist_r));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_eta",m_hist_eta));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_phi",m_hist_phi));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_strip",m_hist_strip));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_layer",m_hist_layer));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_station",m_hist_station));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_all_station",m_hist_sp_station));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_all_layer",m_hist_sp_layer));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_all_module",m_hist_sp_module));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_all_plane",m_hist_sp_plane));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_all_row",m_hist_sp_row));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_all_sensor",m_hist_sp_sensor));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x_z",m_hist_x_z));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_y_z",m_hist_y_z));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x_y_plane0",m_hist_x_y_plane0));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x_y_plane1",m_hist_x_y_plane1));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x_y_plane2",m_hist_x_y_plane2));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x_y_plane3",m_hist_x_y_plane3));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x_y_plane4",m_hist_x_y_plane4));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x_y_plane5",m_hist_x_y_plane5));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x_y_plane6",m_hist_x_y_plane6));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x_y_plane7",m_hist_x_y_plane7));
  CHECK(m_thistSvc->regHist("/TruthTrackSeeds/sp/sp_x_y_plane8",m_hist_x_y_plane8));
  ATH_MSG_INFO( "TruthTrackSeeds::initialized for package version " << PACKAGE_VERSION );
  return StatusCode::SUCCESS;
}

//-------------------------------------------------------------------------

StatusCode TruthSeededTrackFinder::execute (const EventContext& ctx) const
{


  ++m_numberOfEvents;

  // Handles created from handle keys behave like pointers to the corresponding container
  SG::ReadHandle<McEventCollection> h_mcEvents(m_mcEventKey, ctx);
  ATH_MSG_INFO("Read McEventContainer with " << h_mcEvents->size() << " events");
  if (h_mcEvents->size() == 0) return StatusCode::FAILURE;

  SG::ReadHandle<FaserSiHitCollection> h_siHits(m_faserSiHitKey, ctx);
  ATH_MSG_INFO("Read FaserSiHitCollection with " << h_siHits->size() << " hits");

  SG::ReadHandle<FaserSCT_RDO_Container> h_sctRDO(m_faserRdoKey, ctx);


  SG::WriteHandle<SpacePointContainer> seedContainer_SCT(m_seed_spcontainerKey, ctx );

  ATH_CHECK( seedContainer_SCT.record( std::make_unique<SpacePointContainer>(m_idHelper->wafer_hash_max()) ) );
  ATH_MSG_DEBUG("Created SpacePointContainer " << m_seed_spcontainerKey.key() << " N= " << m_idHelper->wafer_hash_max());


  const TrackerDD::SiDetectorElementCollection* elements = nullptr;
  SG::ReadCondHandle<TrackerDD::SiDetectorElementCollection> sctDetEle(m_SCTDetEleCollKey, ctx);
  elements = sctDetEle.retrieve();
  if (elements==nullptr) {
    ATH_MSG_FATAL("Pointer of SiDetectorElementCollection (" << m_SCTDetEleCollKey.fullKey() << ") could not be retrieved");
    return StatusCode::SUCCESS;
  }

  // register the IdentifiableContainer into StoreGate


  // retrieve SCT cluster container
  SG::ReadHandle<SpacePointContainer> sct_spcontainer( m_Sct_spcontainerKey, ctx );
  if (!sct_spcontainer.isValid()){
    msg(MSG:: FATAL) << "Could not find the data object "<< sct_spcontainer.name() << " !" << endmsg;
    return StatusCode::RECOVERABLE;
  }


  ATH_MSG_DEBUG( "SCT spacepoint container found: " << sct_spcontainer->size() << " collections" );
  SpacePointContainer::const_iterator it = sct_spcontainer->begin();
  SpacePointContainer::const_iterator itend = sct_spcontainer->end();

  //use 0 as the hash
  IdentifierHash idHash(0);
  Identifier id(0);
  auto spCollection = std::make_unique<SpacePointCollection>(idHash);
  spCollection->setIdentifier(id);
  spCollection->reserve(100);
  SpacePointContainer::IDC_WriteHandle lock = seedContainer_SCT->getWriteHandle(idHash);

  int nTruthSP=0;
  ATH_MSG_DEBUG( "Start iteration of spacepoint collections"  );
  for (; it != itend; ++it){
    ++m_numberOfSPCollection;
    const SpacePointCollection *colNext=&(**it);
    int nReceivedSPSCT = colNext->size();

    // Create SpacePointCollection
    IdentifierHash idHash = colNext->identifyHash();
    Identifier elementID = colNext->identify();
    size_t size = colNext->size();
    ATH_MSG_DEBUG( "Spacepoint collection in iteraction: size =  " <<nReceivedSPSCT <<", IDHash = "<<idHash<<", ID = "<<elementID );
    if (size == 0){
      ATH_MSG_VERBOSE( "Algorithm found no space points" );
      ++m_numberOfEmptySPCollection;
    } else {
      //In a MT environment the cache maybe filled by another thread in which case this will delete the duplicate
      SpacePointCollection::const_iterator sp_begin= colNext->begin();
      SpacePointCollection::const_iterator sp_end= colNext->end();
      ATH_MSG_DEBUG( "Iterate the spacepoint in collection "  );
      for (; sp_begin != sp_end; ++sp_begin){
	++m_numberOfSP;
	const Trk::SpacePoint* sp=&(**sp_begin);
	bool match=false;
	//get truth hit
	const auto identifier = sp->clusterList().first->identify();
	int station = m_idHelper->station(identifier);
	int plane = m_idHelper->layer(identifier);
	int row = m_idHelper->phi_module(identifier);
	int module = m_idHelper->eta_module(identifier);
	int sensor = m_idHelper->side(identifier);
	m_hist_sp_station->Fill(station);
	m_hist_sp_plane->Fill(plane);
	m_hist_sp_layer->Fill((station-1)*3+plane);
	m_hist_sp_row->Fill(row);
	m_hist_sp_module->Fill(module);
	m_hist_sp_sensor->Fill(sensor);

	ATH_MSG_DEBUG( "Spacepoint :"<<identifier  );
        SG::ReadHandle<TrackerSimDataCollection> h_collectionMap(m_sctMap, ctx);
	ATH_MSG_DEBUG("map size "<<h_collectionMap->size());
	if( h_collectionMap->count(identifier) == 0)
	{
	  ATH_MSG_INFO("no map found w/identifier "<<identifier);
	  ++m_numberOfNoMap;
	  continue;
	}

	//Collection map takes identifier and returns simulation data
	const auto& simdata = h_collectionMap->find(identifier)->second;
	const auto& deposits = simdata.getdeposits();

	//loop through deposits to find one w/ highest energy & get barcode
	float highestDep = 0;
	int barcode = 0;
	for( const auto& depositPair : deposits)
	{
	  if( depositPair.second > highestDep)
	  {
	    highestDep = depositPair.second;
	    barcode = depositPair.first->barcode();
	    depositPair.first->print(std::cout);
	    ATH_MSG_INFO("pdg id "<<depositPair.first->pdg_id());
	  }
	}
	ATH_MSG_INFO("final barcode of: "<<barcode);
	if(barcode%1000000 != 10001)
	  continue;

	//Helper function to get hit location information from RDO identifier
	ATH_MSG_INFO("trying to match hit to stat/plane/row/mod/sens: "<<station<<" "<<plane<<" "<<row<<" "<<module<<" "<<sensor);
	for (const FaserSiHit& hit : *h_siHits)
	{
	  ++m_numberOfHits;
	  ATH_MSG_INFO("hit w/vals "<<hit.getStation()<<" "<<hit.getPlane()<<" "<<hit.getRow()<<" "<<hit.getModule()<<" "<<hit.getSensor()<<" barcode: "<<hit.trackNumber());
	  //set of conditions to confirm looking at same particle in same place for SiHit as RDO
	  if(hit.getStation() == station 
	      && hit.getPlane() == plane
	      && hit.getRow() == row
	      && hit.getModule() == module
	      && hit.getSensor() == sensor
	      && hit.trackNumber() == barcode)
	  {
	    ++m_numberOfMatchSP;
	    ATH_MSG_INFO("matched particle and plotting w/ barcode "<<barcode);
	    match=true;
	  }
	}
//	//
//
	if(!match)
	  continue;
//
	++m_numberOfFills;
	++nTruthSP;
// fails, don't know why
//	spCollection->push_back(const_cast<TrackerSpacePoint*>(sp));

	m_hist_r->Fill(sp->r());
	m_hist_eta->Fill(sp->eta());
	m_hist_phi->Fill(sp->phi());
	Amg::Vector3D gloPos=sp->globalPosition();
	m_hist_x->Fill(gloPos.x());
	m_hist_y->Fill(gloPos.y());
	m_hist_z->Fill(gloPos.z());
	m_hist_strip->Fill(m_idHelper->strip(elementID));
	m_hist_station->Fill(m_idHelper->station(elementID));
	m_hist_layer->Fill(m_idHelper->layer(elementID));
	m_hist_x_z->Fill(gloPos.x(),gloPos.z());
	m_hist_y_z->Fill(gloPos.y(),gloPos.z());
	int ilayer=m_idHelper->layer(elementID);
	int istation=m_idHelper->station(elementID);
	if ( ((istation-1)*3+ilayer) == 0 ) m_hist_x_y_plane0->Fill(gloPos.x(),gloPos.y());
	if ( ((istation-1)*3+ilayer) == 1 ) m_hist_x_y_plane1->Fill(gloPos.x(),gloPos.y());
	if ( ((istation-1)*3+ilayer) == 2 ) m_hist_x_y_plane2->Fill(gloPos.x(),gloPos.y());
	if ( ((istation-1)*3+ilayer) == 3 ) m_hist_x_y_plane3->Fill(gloPos.x(),gloPos.y());
	if ( ((istation-1)*3+ilayer) == 4 ) m_hist_x_y_plane4->Fill(gloPos.x(),gloPos.y());
	if ( ((istation-1)*3+ilayer) == 5 ) m_hist_x_y_plane5->Fill(gloPos.x(),gloPos.y());
	if ( ((istation-1)*3+ilayer) == 6 ) m_hist_x_y_plane6->Fill(gloPos.x(),gloPos.y());
	if ( ((istation-1)*3+ilayer) == 7 ) m_hist_x_y_plane7->Fill(gloPos.x(),gloPos.y());
	if ( ((istation-1)*3+ilayer) == 8 ) m_hist_x_y_plane8->Fill(gloPos.x(),gloPos.y());
      }
      ATH_MSG_VERBOSE( size << " SpacePoints successfully added to Container !" );
    }
  }

	m_hist_n->Fill(nTruthSP);
  StatusCode sc= lock.addOrDelete( std::move(spCollection) );
  if (sc.isFailure()){
    ATH_MSG_ERROR( "Failed to add SpacePoints to container" );
    return StatusCode::RECOVERABLE;
  }

  return StatusCode::SUCCESS;
}

//---------------------------------------------------------------------------
StatusCode TruthSeededTrackFinder::finalize()
{
  ATH_MSG_INFO( "Finalizing" );
  ATH_MSG_INFO( m_numberOfEvents << " events processed" );
  ATH_MSG_INFO( m_numberOfSPCollection << " spacepoint collections processed" );
  ATH_MSG_INFO( m_numberOfEmptySPCollection<< " spacepoint collections empty" );
  ATH_MSG_INFO( m_numberOfSP<< " sct SP collections processed" );
  ATH_MSG_INFO( m_numberOfNoMap<< " not maped spacepoint" );
  ATH_MSG_INFO( m_numberOfHits<< " sim hits" );
  ATH_MSG_INFO( m_numberOfMatchSP<< " matched spacepoint" );
  ATH_MSG_INFO( m_numberOfFills<< " spacepoint saved" );
  return StatusCode::SUCCESS;
}

//--------------------------------------------------------------------------

}
