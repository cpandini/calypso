/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FaserSCT_GeoModel_SCT_BarrelParameters_H
#define FaserSCT_GeoModel_SCT_BarrelParameters_H

#include <string>

class SCT_DataBase;

class SCT_BarrelParameters {

public:

  // Constructor 
  SCT_BarrelParameters(SCT_DataBase* rdb);

  // Barrel General

  double etaHalfPitch() const;
  double phiHalfPitch() const;
  double depthHalfPitch() const;
  double sideHalfPitch() const;

  int    numLayers() const;
  double layerPitch() const;
  double phiStagger() const;
  
private:
  SCT_DataBase * m_rdb;

};


#endif // FaserSCT_GeoModel_SCT_BarrelParameters_H
