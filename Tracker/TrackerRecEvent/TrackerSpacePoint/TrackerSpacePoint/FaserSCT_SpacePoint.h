/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// SCT_SpacePoint.h
//   Header file for class SCT_SpacePoint
///////////////////////////////////////////////////////////////////
// Class to handle SPs for SCT
///////////////////////////////////////////////////////////////////
// Version 1.0   12/12/2005 Martin Siebel
///////////////////////////////////////////////////////////////////
#ifndef TRACKERSPACEPOINT_SCTSPACEPOINT_H
#define TRACKERSPACEPOINT_SCTSPACEPOINT_H

#include "TrkSpacePoint/SpacePoint.h"
#include "Identifier/IdentifierHash.h"

namespace Trk
{
  class TrackerCluster;
}

namespace Tracker
{
  /**
   * @class SCT_SpacePoint
   * An SCT_SpacePoint is created from two SCT_Cluster's from two different wafers.
   */
  
  class FaserSCT_SpacePoint : public Trk::SpacePoint 
    {
      
      /////////////////////////////`//////////////////////////////////////
      // Public methods:
      ///////////////////////////////////////////////////////////////////

    public:
      
      /** Default constructor */
      FaserSCT_SpacePoint() ;

      /**
       * @name Parametrised constructors
       * In order to ensure initialisation, the global Position has to be
       * on the surface associated to the FIRST member of the PRD-pair clusList.
       */
      //@{
      FaserSCT_SpacePoint(const std::pair<IdentifierHash, IdentifierHash>& elementIdList, 
		     const Amg::Vector3D* position, 
		     const std::pair<const Trk::PrepRawData*, const Trk::PrepRawData*>* clusList) ;
      
      FaserSCT_SpacePoint(const std::pair<IdentifierHash, IdentifierHash>& elementIdList, 
		     const Amg::Vector3D* position,
		     const Amg::MatrixX* loccov,//assumes ownership of loccov
		     const std::pair<const Trk::PrepRawData*, const Trk::PrepRawData*>* clusList) ;
      
      FaserSCT_SpacePoint(const std::pair<IdentifierHash, IdentifierHash>& elementIdList, 
		     const Amg::Vector3D& position, 
		     const std::pair<const Trk::PrepRawData*, const Trk::PrepRawData*>* clusList) ;
      
      FaserSCT_SpacePoint(const std::pair<IdentifierHash, IdentifierHash>& elementIdList, 
		     const Amg::Vector3D& position,
		     const Amg::MatrixX& loccov,//assumes ownership of loccov
		     const std::pair<const Trk::PrepRawData*, const Trk::PrepRawData*>* clusList) ;
      //@}

      /** Copy Constructor */
      FaserSCT_SpacePoint(const FaserSCT_SpacePoint &) ;

      /** Destructor */
      virtual ~FaserSCT_SpacePoint() = default;

      /** Overloading Assignment Operator */
      FaserSCT_SpacePoint &operator=(const FaserSCT_SpacePoint &);
      
      /** Clones */
      virtual Trk::SpacePoint* clone() const ;       
      
      /**Interface method for output, to be overloaded by child classes* */
      virtual MsgStream&    dump( MsgStream& out ) const ;  

      /**Interface method for output, to be overloaded by child classes* */
      virtual std::ostream& dump( std::ostream& out ) const ;
      
    private:

      /** method to set up the local Covariance Matrix. */
      void setupLocalCovarianceSCT() ;

      /** common method used in constructors. */
      void setup(const std::pair<IdentifierHash, IdentifierHash>& elementIdList,  		    
		 const Amg::Vector3D& position,
		 const std::pair<const Trk::PrepRawData*, const Trk::PrepRawData*>* clusList);
    };
  
  ///////////////////////////////////////////////////////////////////
  // Inline methods:
  ///////////////////////////////////////////////////////////////////

  inline Trk::SpacePoint* FaserSCT_SpacePoint::clone() const
    {  return new FaserSCT_SpacePoint(*this) ;  }
  
}
#endif // TRACKERSPACEPOINT_SCTSPACEPOINT_H
