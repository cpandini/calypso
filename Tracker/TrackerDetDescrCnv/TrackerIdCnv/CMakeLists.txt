################################################################################
# Package: TrackerIdCnv
################################################################################

# Declare the package name:
atlas_subdir( TrackerIdCnv )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PRIVATE
                          Control/StoreGate
                          DetectorDescription/DetDescrCnvSvc
                          DetectorDescription/TrackerDetDescr
                          GaudiKernel
                          Tracker/TrackerDetDescr/TrackerIdentifier )

# Component(s) in the package:
atlas_add_component( TrackerIdCnv
                     src/*.cxx
                     LINK_LIBRARIES StoreGateLib SGtests DetDescrCnvSvcLib IdDictDetDescr GaudiKernel TrackerIdentifier )

# Install files from the package:
atlas_install_joboptions( share/*.py )

