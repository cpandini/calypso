// System include(s):
#include <iostream>

// Local include(s):
#include "FaserTruthAccessors.h"

/// Helper macro for managing cluster moment Accessor objects
#define DEFINE_ACCESSOR( PARENT, TYPE, NAME )                \
   case PARENT::NAME:                                        \
      {                                                      \
         static SG::AuxElement::Accessor< TYPE > a( #NAME ); \
         return &a;                                          \
      }                                                      \
      break

namespace xAOD {

   SG::AuxElement::Accessor< float >*
   polarizationAccessor( FaserTruthParticle::PolParam type ) {

      switch( type ) {

         DEFINE_ACCESSOR( FaserTruthParticle, float, polarizationTheta );
         DEFINE_ACCESSOR( FaserTruthParticle, float, polarizationPhi );

      default:
         std::cerr << "xAOD::polarizationAccessor ERROR Unknown PolParam ("
                   << type << ") requested" << std::endl;
         return 0;
      }

      return 0;
   }

   SG::AuxElement::Accessor< int >*
   pdfInfoAccessorInt( FaserTruthEvent::PdfParam type ) {

      switch( type ) {

         DEFINE_ACCESSOR( FaserTruthEvent, int, PDGID1 );
         DEFINE_ACCESSOR( FaserTruthEvent, int, PDGID2 );
         DEFINE_ACCESSOR( FaserTruthEvent, int, PDFID1 );
         DEFINE_ACCESSOR( FaserTruthEvent, int, PDFID2 );

      default:
         std::cerr << "xAOD::pdfInfoAccessorInt ERROR Unknown PdfParam ("
                   << type << ") requested" << std::endl;
         return 0;
      }

      return 0;
   }

   SG::AuxElement::Accessor< float >*
   pdfInfoAccessorFloat( FaserTruthEvent::PdfParam type ) {

      switch( type ) {

         DEFINE_ACCESSOR( FaserTruthEvent, float, X1 );
         DEFINE_ACCESSOR( FaserTruthEvent, float, X2 );
         DEFINE_ACCESSOR( FaserTruthEvent, float, Q );
         DEFINE_ACCESSOR( FaserTruthEvent, float, XF1 );
         DEFINE_ACCESSOR( FaserTruthEvent, float, XF2 );

      default:
         std::cerr << "xAOD::pdfInfoAccessorFloat ERROR Unknown ParamDef ("
                   << type << ") requested" << std::endl;
         return 0;
      }

      return 0;
   }

} // namespace xAOD
