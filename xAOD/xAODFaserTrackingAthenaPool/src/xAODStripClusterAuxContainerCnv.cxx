// System include(s):
#include <exception>

// Local include(s):
#include "xAODStripClusterAuxContainerCnv.h"
#include "AthContainers/tools/copyAuxStoreThinned.h"
#include "AthenaKernel/ThinningDecisionBase.h"

xAODStripClusterAuxContainerCnv::
xAODStripClusterAuxContainerCnv( ISvcLocator* svcLoc, const char* name )
   : xAODStripClusterAuxContainerCnvBase( svcLoc, name ) {

}

xAOD::StripClusterAuxContainer*
xAODStripClusterAuxContainerCnv::
createPersistentWithKey( xAOD::StripClusterAuxContainer* trans, const std::string& /*key*/ ) {

   // Create a copy of the container:
   if (static_cast<const SG::IAuxStore&>(*trans).size() > 0)
   {
      auto pers = new xAOD::StripClusterAuxContainer();
      SG::copyAuxStoreThinned(*trans, *pers, static_cast<SG::ThinningDecisionBase*>(nullptr));
      return pers;
   }
   return new xAOD::StripClusterAuxContainer(*trans);
}

xAOD::StripClusterAuxContainer*
xAODStripClusterAuxContainerCnv::createTransientWithKey(const std::string& /*key*/) {

   // The known ID(s) for this container:
   static const pool::Guid v1_guid( "61B62A1A-4C51-43A2-8364-1B9E910A81E8" );

   // Check which version of the container we're reading:
   if( compareClassGuid( v1_guid ) ) {
      // It's the latest version, read it directly:
      return poolReadObject< xAOD::StripClusterAuxContainer >();
   }

   // If we didn't recognise the ID:
   throw std::runtime_error( "Unsupported version of "
                             "xAOD::StripClusterAuxContainer found" );
   return 0;
}