// System include(s):
#include <iostream>

// Local include(s):
#include "xAODFaserTracking/StripClusterContainer.h"
#include "xAODFaserTracking/StripClusterAuxContainer.h"

template< typename T >
std::ostream& operator<< ( std::ostream& out,
                           const std::vector< T >& vec ) {

   out << "[";
   for( size_t i = 0; i < vec.size(); ++i ) {
      out << vec[ i ];
      if( i < vec.size() - 1 ) {
         out << ", ";
      }
   }
   out << "]";
   return out;
}

/// Function filling one StripCluster with information
void fill( xAOD::StripCluster& sc ) {

   sc.setLocalPosition( 1.0, 2.0);

   sc.setLocalPositionError( 0.2, 0.2, 0.05 );

   sc.setGlobalPosition( 225.0, -151.4, 144.4 );

   return;
}

/// Function printing the properties of a StripCluster
void print( const xAOD::StripCluster& sc ) {
    
   std::cout << "id = " << sc.id() << std::endl;
   std::cout << "local x = " << sc.localX() << ", local y = " << sc.localY()
             << ", local x error = " << sc.localXError() << ", local y error = " << sc.localYError()
             << ", local xy correlation = " << sc.localXYCorrelation() << std::endl;
    std::cout << "global x = " << sc.globalX() << ", global y = " << sc.globalY() << ", global z = " << sc.globalZ() << std::endl;

   return;
}

int main() {

   // Create the main containers to test:
   xAOD::StripClusterAuxContainer aux;
   xAOD::StripClusterContainer tpc;
   tpc.setStore( &aux );

   // Add one strip cluster to the container:
   xAOD::StripCluster* p = new xAOD::StripCluster();
   tpc.push_back( p );

   // Fill it with information:
   fill( *p );

   // Print the information:
   print( *p );

   // Print the contents of the auxiliary store:
   aux.dump();

   // Return gracefully:
   return 0;
}