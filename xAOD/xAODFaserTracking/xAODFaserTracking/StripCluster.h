/*
  FASER Collaboration
*/
#ifndef XAODFASERTRACKING_STRIPCLUSTER_H
#define XAODFASERTRACKING_STRIPCLUSTER_H
#ifndef STRIPCLUSTER_H
#define STRIPCLUSTER_H

#include <iostream>
#include <vector>
#include <Eigen/Dense>

// Core include(s):
#include "AthContainers/AuxElement.h"

using namespace Eigen;
using namespace std;

namespace xAOD {
    
    class StripCluster : public SG::AuxElement {
    
    public:
    
        /// Default constructor
        StripCluster();
        
        /// Returns the identifier
        uint64_t id() const;
        /// Returns the list of RDO identifiers
        const std::vector<uint64_t>& rdoIdentifierList() const;
        /// Sets the identifier
        void setId(uint64_t id);
        /// Sets the list of RDO identifiers
        void setRdoIdentifierList(const std::vector< uint64_t >& rdoIdentifierList);
        
        /// @name Local position functions
        /// Returns the local position
        /// @{
        /// Returns the x position
        float localX() const;
        /// Returns the y position
        float localY() const;
        /// Returns the x position error
        float localXError() const;
        /// Returns the y position error
        float localYError() const;
        /// Returns the xy position correlation
        float localXYCorrelation() const;
        /// Sets the local position
        void setLocalPosition(float localX, float localY);
        /// Sets the local position error
        void setLocalPositionError(float localXError, float localYError, float localXYCorrelation);
        /// @}
        /// @name Global position functions
        /// Returns the global position
        /// @{
        /// Returns the x position
        float globalX() const;
        /// Returns the y position
        float globalY() const;
        /// Returns the z position
        float globalZ() const;
        /// Sets the global position
        void setGlobalPosition(float globalX, float globalY, float globalZ);
        /// @}
    
    };
    #endif // STRIPCLUSTER_H

} // end of the xAOD namespace
#endif // XAODFASERTRACKING_STRIPCLUSTER_H