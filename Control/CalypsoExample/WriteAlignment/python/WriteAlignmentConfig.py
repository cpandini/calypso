# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#!/usr/bin/env python
import sys
from AthenaCommon.Constants import VERBOSE
from AthenaConfiguration.ComponentFactory import CompFactory

def WriteAlignmentCfg(flags, name="WriteAlignmentAlg", **kwargs):

    # Initialize GeoModel
    from FaserGeoModel.FaserGeoModelConfig import FaserGeometryCfg
    a = FaserGeometryCfg(flags)

    # This section is to allow alignment to be written to a conditions DB file
    from IOVDbSvc.IOVDbSvcConfig import IOVDbSvcCfg
    result = IOVDbSvcCfg(flags)
    iovDbSvc = result.getPrimary()
    iovDbSvc.dbConnection="sqlite://;schema=ALLP200.db;dbname=OFLP200"
    a.merge(result)

    AthenaPoolCnvSvc=CompFactory.AthenaPoolCnvSvc
    apcs=AthenaPoolCnvSvc()
    a.addService(apcs)
    EvtPersistencySvc=CompFactory.EvtPersistencySvc
    a.addService(EvtPersistencySvc("EventPersistencySvc",CnvServices=[apcs.getFullJobOptName(),]))

    a.addService(CompFactory.IOVRegistrationSvc(PayloadTable=False,OutputLevel=VERBOSE))

    # Configure the algorithm itself
    WriteAlignmentAlg = CompFactory.WriteAlignmentAlg
    outputTool = CompFactory.AthenaOutputStreamTool("DbStreamTool", OutputFile = "FaserSCT_AlignDb.pool.root")
    kwargs.setdefault("AlignDbTool", CompFactory.TrackerAlignDBTool("AlignDbTool", OutputTool = outputTool))
    a.addEventAlgo(WriteAlignmentAlg(name, **kwargs))

    return a


if __name__ == "__main__":
    # from AthenaCommon.Logging import log, logging
    from AthenaCommon.Configurable import Configurable
    # from AthenaConfiguration.ComponentFactory import CompFactory
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags

    Configurable.configurableRun3Behavior = True
    
# Flags for this job
    ConfigFlags.Input.isMC = True                                # Needed to bypass autoconfig
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-XXXX-XXX-XX"          # Needed to bypass autoconfig, only the "OFLCOND" matters at the moment
    ConfigFlags.GeoModel.FaserVersion     = "FASER-01"           # Default FASER geometry
    ConfigFlags.GeoModel.GeoExportFile    = "faserGeo.db"        # Writes out a GeoModel file with the full geometry tree (optional, comment out to skip)
    ConfigFlags.Detector.SimulateVeto     = True
    ConfigFlags.Detector.SimulateTrigger  = True
    ConfigFlags.Detector.SimulatePreshower= True
    ConfigFlags.Detector.SimulateFaserSCT = True
    ConfigFlags.Detector.SimulateUpstreamDipole = True
    ConfigFlags.Detector.SimulateCentralDipole = True
    ConfigFlags.Detector.SimulateDownstreamDipole = True
    ConfigFlags.lock()

# Configure components
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(ConfigFlags)

# Set things up to create a conditions DB with neutral Tracker alignment transforms
    acc.merge(WriteAlignmentCfg(ConfigFlags))

# Configure verbosity    
    # ConfigFlags.dump()
    # logging.getLogger('forcomps').setLevel(VERBOSE)
    # acc.foreach_component("*").OutputLevel = VERBOSE
    # acc.foreach_component("*ClassID*").OutputLevel = INFO
    # log.setLevel(VERBOSE)
    
# Execute and finish
    sys.exit(int(acc.run(maxEvents=1).isFailure()))
