#
# Run this with
# athena.py JO_TestConverter.py
#
# Not reading anything, so don't set up EventSelector
# See here for other useful stuff: 
# https://twiki.cern.ch/twiki/bin/view/Main/AthenaCodeSnippets
#import AthenaRootComps.ReadAthenaRoot
#svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/work/r/rjansky/FASER/clusters.root"]
#svcMgr.EventSelector.TupleName = "clusters"

#print svcMgr.EventSelector

# Define number of events to run
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags as af
af.EvtMax=100

# Access the algorithm sequence:
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from LegacyConverter.LegacyConverterConf import Legacy__FaserTestConverter

# Add the algorithm.
importalg = Legacy__FaserTestConverter("FaserTestConverter")
importalg.InputFileName = "CaloPbC5cm_1k.root"
importalg.OutputLevel = VERBOSE
topSequence += importalg
print importalg

from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
xaodStream = MSMgr.NewPoolRootStream( "StreamAOD", "XAOD_"+"clusters"+".pool.root" )

#xaodStream.AddItem("xAOD::FaserTruthEventContainer#*")
#xaodStream.AddItem("xAOD::FaserTruthEventAuxContainer#*")
#xaodStream.AddItem("xAOD::FaserTruthParticleContainer#*")
#xaodStream.AddItem("xAOD::FaserTruthParticleAuxContainer#*")
#xaodStream.AddItem("xAOD::FaserTruthVertexContainer#*")
#xaodStream.AddItem("xAOD::FaserTruthVertexAuxContainer#*")

xaodStream.AddItem("xAOD::StripClusterContainer#*")
xaodStream.AddItem("xAOD::StripClusterAuxContainer#*")
