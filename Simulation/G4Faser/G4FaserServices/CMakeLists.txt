################################################################################
# Package: G4FaserServices
################################################################################

# Declare the package name:
atlas_subdir( G4FaserServices )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          GaudiKernel
                          PRIVATE
                          Control/AthenaBaseComps
                          MagneticField/MagFieldInterfaces
                          Generators/GeneratorObjects
                          Simulation/G4Atlas/G4AtlasInterfaces
                          Simulation/G4Atlas/G4AtlasTools
                          Simulation/G4Utilities/G4PhysicsLists)

# External dependencies:
find_package( CLHEP )
find_package( Geant4 )
find_package( TBB )
find_package( XercesC)

# Component(s) in the package:
#atlas_add_component( G4FaserServices
#                     src/*.cxx
#                     src/components/*.cxx
#                     INCLUDE_DIRS ${GEANT4_INCLUDE_DIRS} ${XERCESC_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${TBB_INCLUDE_DIRS}
#                     LINK_LIBRARIES ${GEANT4_LIBRARIES} ${XERCESC_LIBRARIES} ${CLHEP_LIBRARIES} ${TBB_LIBRARIES} GaudiKernel AthenaBaseComps G4AtlasInterfaces G4AtlasToolsLib G4PhysicsLists MagFieldInterfaces )


# Install files from the package:
atlas_install_python_modules( python/*.py )
atlas_install_scripts( test/*.py )
