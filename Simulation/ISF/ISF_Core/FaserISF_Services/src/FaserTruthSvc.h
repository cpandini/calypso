/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// FaserTruthSvc.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef FASERISF_SERVICES_FASERTRUTHSVC_H
#define FASERISF_SERVICES_FASERTRUTHSVC_H 1

// STL includes
#include <string>

// FrameWork includes
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "AthenaBaseComps/AthService.h"

// ISF include
#include "FaserISF_Interfaces/IFaserTruthSvc.h"
#include "FaserISF_Event/IFaserTruthIncident.h"

// DetectorDescription
#include "FaserDetDescr/FaserRegion.h"

// Barcode
#include "BarcodeEvent/Barcode.h"

// McEventCollection
#include "GeneratorObjects/McEventCollection.h"

// forward declarations
class StoreGateSvc;

namespace Barcode {
  class IBarcodeSvc;
}

namespace HepMC {
  class GenEvent;
}

namespace ISFTesting {
  class TruthSvc_test;
}

namespace ISF {

  class IFaserTruthStrategy;
  typedef ToolHandleArray<IFaserTruthStrategy>     FaserTruthStrategyArray;

  /** @class FaserTruthSvc

      HepMC based version of the ISF::ITruthSvc,
      currently it takes an IFaserTruthIncident base class


      @author Andreas.Salzburger -at- cern.ch , Elmar.Ritsch -at- cern.ch
  */
  class FaserTruthSvc final : public extends<AthService, IFaserTruthSvc> {

    // allow test to access private data
    friend ISFTesting::TruthSvc_test;

  public:

    //** Constructor with parameters */
    FaserTruthSvc( const std::string& name, ISvcLocator* pSvcLocator );

    /** Destructor */
    virtual ~FaserTruthSvc();

    /** Athena algorithm's interface method initialize() */
    StatusCode  initialize() override;
    /** Athena algorithm's interface method finalize() */
    StatusCode  finalize() override;

    /** Register a truth incident */
    void registerTruthIncident( IFaserTruthIncident& truthincident) const override;

    /** Initialize the Truth Svc at the beginning of each event */
    StatusCode initializeTruthCollection() override;

    /** Finalize the Truth Svc at the end of each event*/
    StatusCode releaseEvent() override;

  private:
    /** Record the given truth incident to the MC Truth */
    void recordIncidentToMCTruth( IFaserTruthIncident& truthincident) const;
    /** Record and end vertex to the MC Truth for the parent particle */
    HepMC::GenVertex *createGenVertexFromTruthIncident( IFaserTruthIncident& truthincident,
                                                        bool replaceExistingGenVertex=false) const;

    /** Set shared barcode for child particles */
    void setSharedChildParticleBarcode( IFaserTruthIncident& truthincident) const;

    /** Delete child vertex */
    void deleteChildVertex(HepMC::GenVertex* vertex) const;

    /** Helper function to determine the largest particle barcode set by the generator */
    int maxGeneratedParticleBarcode(HepMC::GenEvent *genEvent) const;

    /** Helper function to determine the largest vertex barcode set by the generator */
    int maxGeneratedVertexBarcode(HepMC::GenEvent *genEvent) const;

    ServiceHandle<Barcode::IBarcodeSvc>       m_barcodeSvc;           //!< The Barcode service

    /** the truth strategies applied (as AthenaToolHandle Array) */
    FaserTruthStrategyArray                   m_truthStrategies;
    /** for faster access: using an internal pointer to the actual ITruthStrategy instances */
    IFaserTruthStrategy**                     m_geoStrategies[FaserDetDescr::fNumFaserRegions];
    unsigned short                            m_numStrategies[FaserDetDescr::fNumFaserRegions];

    /** MCTruth steering */
    bool                                      m_skipIfNoChildren;       //!< do not record incident if numChildren==0
    bool                                      m_skipIfNoParentBarcode;  //!< do not record if parentBarcode==fUndefinedBarcode
    bool                                      m_ignoreUndefinedBarcodes;//!< do/don't abort if retrieve an undefined barcode

    bool                                      m_passWholeVertex;

    std::vector<int>                         m_forceEndVtxRegionsVec; //!< property containing AtlasRegions for which
                                                                              //   to write end-vtx
    bool                                      m_forceEndVtx[FaserDetDescr::fNumFaserRegions]; //!< attach end vertex to
                                                                                                     //   all parent particles if they die

    bool                                      m_quasiStableParticlesIncluded; //!< does this job simulate quasi-stable particles.

  };
}


#endif //> !FASERISF_SERVICES_FASERTRUTHSVC_H
